from Bike import Bike
#Import the 'Bike' class from the file Bike.py
from HighFrame import HighFrame
#Import the HighFrame class from HighFrame.py

class CityBike(Bike):
#A sub-class of Bike that represents a CityBike object
    
    def __init__(self):
        #Assign attribute name with value 'CityBike'
        #to CityBike object
        self._name = "CityBike"
        #Pass the CityBike object into the __init__
        #of the Bike class
        Bike.__init__(self)

    def _hasCarrier(self):
        #Returns True if the CityBike has a carrier.
        return True

    def _hasFrontLight(self):
        #Returns true if the CityBike has a FrontLight
        return True

    def _hasRearLight(self):
        #Returns true if the CityBike has a RearLight
        return True

    def _frameType(self):
        #Assigns the CityBike object a frametype
        _frame = HighFrame()
        #'_frame' becomes a object of the HighFrame class
        self._frame = _frame.__str__()
        #CityBike attribute '_frame_' is the value of the
        #string function of the HighFrame Class.
        return self._frame
        

    def __str__(self):
        #Returns the attribute name in string form
        outstr = ''
        outstr += self._name
        return outstr
