from Bike import Bike
#Import the 'Bike' class from the file Bike.py
from MediumFrame import MediumFrame
#Import the MediumFrame class from MediumFrame.py

class Hybrid(Bike):
#A sub-class of Bike that represents a Hybrid bike object
    
    def __init__(self):
        #Assign attribute name with value 'Hybrid'
        #to Hybrid object
         self._name = "Hybrid"
        #Pass the Hybrid object into the __init__
        #of the Bike class
         Bike.__init__(self)

    def _hasCarrier(self):
        #Returns false if the Hybrid bike has no carrier.
        return False

    def _hasFrontLight(self):
        #Returns true if the Hybrid bike has a FrontLight
        return True

    def _hasRearLight(self):
        #Returns true if the Hybrid bike has a RearLight.
        return True

    def _frameType(self):
        #Assigns the Hybrid object a frametype
        _frame = MediumFrame()
        #'_frame' becomes a object of the LowFrame class
        self._frame = _frame.__str__()
        #Hybrid attribute '_frame_' is the value of the
        #string function of the MediumFrame Class.
        return self._frame
        

    def __str__(self):
        #Returns the attribute name in string form
        outstr = ''
        outstr += self._name
        return outstr
