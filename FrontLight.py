class FrontLight:
#Class to represent the 'FrontLight' component
    
    def __init__(self):
        #Assign attribute 'name' with value 'FrontLight'
        #to FrontLight object
        self._name = 'FrontLight'

    def __str__(self):
        #Returns the value of attribute 'name' in string form
        outstr = ''
        outstr += self._name
        return outstr
