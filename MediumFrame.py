class MediumFrame:
#A class to represent a MediumFrame frame type object
    
    def __init__(self):
        #Assigns attribute 'name' with value 'MediumFrame'
        #to MediumFrame() object
        self._name = 'MediumFrame'

    def __str__(self):
        #Returns the value of attribute 'name' in string form
        outstr = ''
        outstr += self._name
        return outstr
