class Handlebar:
#A class to represent a Handlebar component
    
    def __init__(self):
        #Assigns attribute 'name' with value 'handlebar'
        #to Handlebar object
        self._name = 'handlebar'

    def __str__(self):
        #Returns the value of attribute 'name' in string form
        outstr = ''
        outstr += self._name
        return outstr
