class Wheels:
#Class to represent the 'Wheel' component
    
    def __init__(self):
        #Assign attribute 'name' to Wheel instance
        self._name = 'wheels'

    def __str__(self):
        #Return the name of the component in string form
        outstr = ''
        outstr += self._name
        return outstr
