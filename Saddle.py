class Saddle:
#Class to represent the 'Saddle' component
    
    def __init__(self):
        #Assign attribute 'name' with value 'saddle' to
        #Saddle object
        self._name = 'saddle'

    def __str__(self):
        #Return the value of the 'name' attribute in string form
        outstr = ''
        outstr += self._name
        return outstr
