from Bike import Bike
#Import the 'Bike' class from the file Bike.py
from LowFrame import LowFrame
#Import the LowFrame class from LowFrame.py

class MountainBike(Bike):
#A sub-class of Bike that represents a MountainBike object

    def __init__(self):
        #Assign attribute name with value 'MountainBike'
        #to MountainBike object
        self._name = "MountainBike"
        #Pass the MountainBike object into the __init__
        #of the Bike class
        Bike.__init__(self)

    def _hasCarrier(self):
        #Returns false if the MountainBike has no carrier.
        return False

    def _hasFrontLight(self):
        #Returns false if the MountainBike has no frontlight.
        return False

    def _hasRearLight(self):
        #Returns false if the MountainBike has no rearlight.
        return False

    def _frameType(self):
        #Assigns the MountainBike object a frametype
        _frame = LowFrame()
        #'_frame' becomes a object of the LowFrame class
        self._frame = _frame.__str__()
        #MountainBike attribute '_frame_' is the value of the
        #string function of the LowFrame Class.
        return self._frame
        

    def __str__(self):
        #Returns the attribute name in string form
        outstr = ''
        outstr += self.name
        return outstr
