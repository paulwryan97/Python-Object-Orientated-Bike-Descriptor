class Brakes:
#A class to represent a Brake component
    
    def __init__(self):
        #Assigns attribute 'name' with value 'brakes'
        #to Brakes object
        self._name = 'brakes'

    def __str__(self):
        #Returns the value of attribute 'name' in string form
        outstr = ''
        outstr += self._name
        return outstr
