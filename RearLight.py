class RearLight:
#Class to represent the 'RearLight' component
    
    def __init__(self):
        #Assign attribute 'name' with value 'RearLight'
        #to RearLight object
        self._name = 'RearLight'

    def __str__(self):
        #Returns the value of attribute 'name' in string form
        outstr = ''
        outstr += self._name
        return outstr
