
class Bike:
#A superclass that represents a Bike object.

    def __init__(self):
        _brake = Brakes()
        #Attribute _brake is a Brakes() object
        self._brake = _brake.__str__()
        _wheel = Wheels()
        #Attribute _wheel is a Wheels() object
        self._wheel = _wheel.__str__()
        _saddle = Saddle()
        #Attribute _saddle is a Saddle() object
        self._saddle = _saddle.__str__()
        _handlebar = Handlebar()
        #Attribute _handlebar is a Handlebar() object
        self._handlebar = _handlebar.__str__()
        _carrier = Carrier()
        #Attribute _carrier is a Carrier() object
        self._carrier = _carrier.__str__()
        _frontlight = FrontLight()
        #Attribute _frontlight is a FrontLight() object
        self._frontlight = _frontlight.__str__()
        _rearlight = RearLight()
        #Attribute _rearlight is a RearLight() object
        self._rearlight = _rearlight.__str__()

        
    def printComponents(self):
        #Prints the list of components included in the Bike object
        outstr = self._name + ':' + ' '
        outstr += self._frameType() + ', '
        outstr += self._brake + ', '
        outstr += self._wheel + ', '
        if self._hasCarrier():
            outstr += self._carrier + ', '
        outstr += self._saddle + ', '
        outstr += self._handlebar + ', '
        if self._hasFrontLight():
            outstr += self._frontlight + ', '
        if self._hasRearLight():
            outstr += self._rearlight + '.'
        print(outstr)

        
from Brakes import *
#Imports the Brakes class from Brakes.py
from Wheels import *
#Imports the Wheels class from Wheels.py
from Saddle import *
#Imports the Saddle class from Saddle.py
from Handlebar import *
#Imports the Handlebar class from Handlebar.py
from Carrier import *
#Imports the Carrier class from Carrier.py
from FrontLight import *
#Imports the FrontLight class from Frontlight.py
from RearLight import *
#Imports the RearLight class from RearLight.py
from LowFrame import *
#Imports the LowFrame class from LowFrame.py
from MediumFrame import *
#Imports the MediumFrame class from MediumFrame.py
from HighFrame import *
#Imports the HighFrame class from HighFrame.py
from MountainBike import *
#Imports the MountainBike sub-class from MountainBike.py
from CityBike import *
#Imports the CityBike sub-class from CityBike.py
from Hybrid import *
#Imports the Hybrid sub-class from Hybrid.py



class Main:
    def main():
        bikes = [MountainBike(), CityBike(), Hybrid()]

        for cycle in bikes:
            #print('next: "%s"' % str(bicycle._name))
            Bike.printComponents(cycle)

main = Main()
        
