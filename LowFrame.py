class LowFrame:
#A class to represent a LowFrame frame type object
    
    def __init__(self):
        #Assigns attribute 'name' with value 'LowFrame'
        #to LowFrame object
        self._name = 'LowFrame'

    def __str__(self):
        #Returns the value of attribute 'name' in string form
        outstr = ''
        outstr += self._name
        return outstr
