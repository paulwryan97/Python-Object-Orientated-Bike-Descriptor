class HighFrame:
#A class to represent the HighFrame frame type object
    
    def __init__(self):
        #Assigns attribute 'name' with value 'HighFrame'
        #to HighFrame object
        self._name = 'HighFrame'

    def __str__(self):
        #Returns the value of attribute 'name' in string form
        outstr = ''
        outstr += self._name
        return outstr
