class Carrier:
#A class to represent the Carrier component
    
    def __init__(self):
        #Assigns attribute 'name' with value 'Carrier'
        #to Carrier object
        self._name = 'Carrier'


    def __str__(self):
        #Returns the value of attribute 'name' in string form
        outstr = ''
        outstr += self._name
        return outstr
